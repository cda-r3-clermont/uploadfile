<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 08/09/2023
  Time: 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Les fichiers téléchargés</title>
</head>
<body>

<h1>Les fichiers de ma mediathèque</h1>

<a href="./form">Ajouter un nouveau fichier</a>

<c:forEach items="${files}" var="file">
    <div>
        <h2>${file.nom}</h2>
        <img src="${file.lien}" alt="${file.nom}">
    </div>

</c:forEach>

</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 08/09/2023
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Formulaire d'upload d'image</title>
</head>
<body>
<h1>Upload ton fichier !</h1>
<a href="./files">Voir les fichiers</a>
<form enctype="multipart/form-data" method="post">
    <label>Upload ton fichier</label>
    <input type="file" name="file_upload">
    <input type="submit">

    <ul>
        <c:forEach items="${errors}" var="error">
            <li>${error}</li>
        </c:forEach>

    </ul>
</form>
</body>
</html>

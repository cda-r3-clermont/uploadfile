package com.humanbooster.jakarta.mediatheque.servlets;

import com.humanbooster.jakarta.mediatheque.models.UploadedFile;
import com.humanbooster.jakarta.mediatheque.services.UploadFileService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "files", urlPatterns = "/files")
public class MediaServlet extends HttpServlet {

    private UploadFileService uploadFileService;

    public MediaServlet(){
        super();
        this.uploadFileService = new UploadFileService();

    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<UploadedFile> files = this.uploadFileService.getAll();
        request.setAttribute("files", files);
        request.getRequestDispatcher("list-file.jsp").forward(request, response);
    }
}

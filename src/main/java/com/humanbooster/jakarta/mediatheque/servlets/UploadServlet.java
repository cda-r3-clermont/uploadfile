package com.humanbooster.jakarta.mediatheque.servlets;

import com.humanbooster.jakarta.mediatheque.models.UploadedFile;
import com.humanbooster.jakarta.mediatheque.services.UploadFileService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

// Ne pas oublier de mettre MultipartConfig
@WebServlet(name = "upload", urlPatterns = "/form")
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5)
public class UploadServlet extends HttpServlet {

    private UploadFileService uploadFileService;

    public UploadServlet() {
        super();
        this.uploadFileService = new UploadFileService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("form.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Liste d'erreur vide
        List<String> errors = new ArrayList<>();

        // Je réccupére mon fichier dans la requete
        Part filePart = request.getPart("file_upload");
        // Je réccupére le nom original du fichier
        String fileName = filePart.getSubmittedFileName();
        // Je réccupére le mimetype du fichier pour pouvoir analyser si on a le droit de l'upload
        String type = filePart.getContentType();
        // Je vérifie que le fichier n'est pas trop gros
        long fileSize = filePart.getSize();

        // Vérifier que le fichier est pas trop gros
        long sizeMax =  1024 * 1024 * 5;

        // Je vérifie que c'est OK pour l'upload sinon je remplis ma liste d'erreurs
        if(fileSize>sizeMax){
            errors.add("Le fichier est trop lourd !");
        }
        if(!type.equals("image/jpeg") && !type.equals("image/png")){
            errors.add("Nous acceptons seulement les JPG et PNG");
        }

        // Si cest OK lets go j'upload !
        if(errors.isEmpty()){
            // Je réccupére le chemin complet d'upload
            String applicationPath = request.getServletContext().getRealPath("");
            // constructs path of the directory to save uploaded file
            String uploadFilePath = applicationPath  + "uploads";
            // Je cré le dossier upload si il n'existe pas
            File uploadFolder = new File(uploadFilePath);
            if(!uploadFolder.exists()){
                boolean testCreation = uploadFolder.mkdirs();
            }

            // Je génére un nom de fichier unique pour éviter les collisions
            String uniqFilename = UUID.randomUUID().toString();
            String filePath = uploadFilePath +File.separator+ uniqFilename +"-"+ fileName;

            // J'upload toutes les parties de mon fichier
            for(Part part: request.getParts()){
                if(part != null && part.getSize()>0){
                    part.write(filePath);
                }
            }

            // Je sauvegarde dans ma base de données
            String realFilePath = "uploads/"+uniqFilename +"-"+ fileName;
            UploadedFile file = new UploadedFile(realFilePath, fileName, type);
            this.uploadFileService.addFile(file);

            // Je redirige vers le listing de fichiers
            response.sendRedirect("./files");
        } else {
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("form.jsp").forward(request, response);
        }





    }


}

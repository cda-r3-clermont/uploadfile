package com.humanbooster.jakarta.mediatheque.models;

import jakarta.persistence.*;

@Entity
@Table(name = "file")
public class UploadedFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic
    private String lien;

    @Basic
    private String nom;

    @Basic
    private String type;

    public UploadedFile() {
    }

    public UploadedFile(String lien, String nom, String type) {
        this.lien = lien;
        this.nom = nom;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

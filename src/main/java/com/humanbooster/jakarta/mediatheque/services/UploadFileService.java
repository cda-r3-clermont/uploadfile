package com.humanbooster.jakarta.mediatheque.services;

import com.humanbooster.jakarta.mediatheque.models.UploadedFile;
import com.humanbooster.jakarta.mediatheque.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UploadFileService {
    public void addFile(UploadedFile file){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.persist(file);

        tx.commit();
        session.close();
    }

    public List<UploadedFile> getAll(){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        List<UploadedFile> files = session.createQuery("FROM UploadedFile ", UploadedFile.class).getResultList();

        tx.commit();
        session.close();

        return files;
    }
}
